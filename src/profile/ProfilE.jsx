import React from 'react'
import'./ProfilE.css';
import zahara from "../zahara.jpeg";
export default function ProfilE(props) {
  return (
    <div  >
        
        <div className='container' style={{border: '4px solid black',borderRadius:"15px" ,backgroundColor: 'black',heigth:'50px',width:'30%',display:'flex'}}>
        <div className='image' style={{width:"50%",heigth:'100%',textAlign:'center',alignItems:'center'}} >
        <img src={zahara} style={{ width: '100%',height:'100%' ,borderRadius:'20px'}}  />
            
        </div>
        <div className='deuxieme' style={{width:'50%',heigth:"100%", }}>
          <h3 style={{color:"white"}}>{props.fullname  }</h3>
          <p style={{color:"white"}}>{props.bio}</p>
          <p style={{color:"yellow"}}>{props.profession}</p>
        </div> 
          
        </div>
        
       
        
    </div>
  )
}
